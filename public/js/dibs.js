// Sticky menu
function stickymenu() {

    var origTopCoordinateMenu1 = $('nav.main-nav').offset().top + $('nav.main-nav').outerHeight();
    var origTopCoordinateMenu2 = $('nav.main-nav').offset().top;
    var menuHeight = $('nav.main-nav').outerHeight();


    $(window).scroll(function() {

        if ($('.search-form').hasClass('search-open') || $('.main-nav').hasClass('sub-open')) {
            var top = origTopCoordinateMenu2;
        } else {
            var top = origTopCoordinateMenu1;
        }


        if ($(window).scrollTop() >= top) {
            $('nav.main-nav').addClass('sticky');
            //$('.main-content').css('padding-top', menuHeight);
        } else if ($(window).scrollTop() <= origTopCoordinateMenu2) {
            $('nav.main-nav').removeClass('sticky');
            //$('.main-content').css('padding-top', 0);
        }

    });

    if ($(window).scrollTop() >= origTopCoordinateMenu1) {
        $('nav.main-nav').addClass('sticky');
        //$('.main-content').css('padding-top', menuHeight);
    } else if ($(window).scrollTop() < origTopCoordinateMenu2) {
        $('nav.main-nav').removeClass('sticky');
        //$('.main-content').css('padding-top', 0);
    }

    var position = $(window).scrollTop();

    var lastScrollTop = 0,
        delta = 5;
    $(window).scroll(function() {
        if (!($('.main-nav').hasClass('sub-open')) && !($('.search-form').hasClass('search-open'))) {
            var nowScrollTop = $(this).scrollTop();
            if (Math.abs(lastScrollTop - nowScrollTop) >= delta) {
                if (nowScrollTop > lastScrollTop) {
                    // ACTION ON
                    // SCROLLING DOWN
                    $('.main-nav').removeClass('visible');
                } else {
                    // ACTION ON
                    // SCROLLING UP
                    $('.main-nav').addClass('visible');
                }
                lastScrollTop = nowScrollTop;
            }
        }
    });
}




// Select -- transform
function create_custom_dropdowns() {
    $('select').each(function(i, select) {
        $(this).after('<div class="dropdown ' + ($(this).attr('class') || '') + '" tabindex="0"><span class="current"></span><div class="list"><ul></ul></div></div>');
        var dropdown = $(this).next();
        var options = $(select).find('option');
        var selected = $(this).find('option:selected');
        dropdown.find('.current').html(selected.data('display-text') || selected.text());
        options.each(function(j, o) {
            var display = $(o).data('display-text') || '';
            dropdown.find('ul').append('<li class="option ' + ($(o).is(':selected') ? 'selected' : '') + '" data-value="' + $(o).val() + '" data-display-text="' + display + '">' + $(o).text() + '</li>');
        });
    });
}

$(document).on('click', '.dropdown', function(event) {
    $('.dropdown').not($(this)).removeClass('open');
    $(this).toggleClass('open');
    if ($(this).hasClass('open')) {
        $(this).find('.option').attr('tabindex', 0);
        $(this).find('.selected').focus();
    } else {
        $(this).find('.option').removeAttr('tabindex');
        $(this).focus();
    }
});

// Select -- Close when clicking outside
$(document).on({
    'touchstart': function(event) {
        if ($(event.target).closest('.dropdown').length === 0) {
            $('.dropdown').removeClass('open');
            $('.dropdown .option').removeAttr('tabindex');
        }
        event.stopPropagation();
    }
});

$(document).click(function(event) {
    if ($(event.target).closest('.dropdown').length === 0) {
        $('.dropdown').removeClass('open');
        $('.dropdown .option').removeAttr('tabindex');
    }
    event.stopPropagation();
});

// Select -- Option click
$(document).on('click', '.dropdown .option', function(event) {
    $(this).closest('.list').find('.selected').removeClass('selected');
    $(this).addClass('selected');
    var text = $(this).data('display-text') || $(this).text();
    $(this).closest('.dropdown').find('.current').text(text);
    $(this).closest('.dropdown').prev('select').val($(this).data('value')).trigger('change');
    if (!$(this).parents('.list ul').find('.option:first-child').hasClass('selected')) {
        $(this).parents('.dropdown').addClass('filled');
    } else {
        $(this).parents('.dropdown').removeClass('filled');
    }
});

// Select -- Keyboard events
$(document).on('keydown', '.dropdown', function(event) {
    var focused_option = $($(this).find('.list .option:focus')[0] || $(this).find('.list .option.selected')[0]);
    // Space or Enter
    if (event.keyCode == 32 || event.keyCode == 13) {
        if ($(this).hasClass('open')) {
            focused_option.trigger('click');
        } else {
            $(this).trigger('click');
        }
        return false;
        // Down
    } else if (event.keyCode == 40) {
        if (!$(this).hasClass('open')) {
            $(this).trigger('click');
        } else {
            focused_option.next().focus();
        }
        return false;
        // Up
    } else if (event.keyCode == 38) {
        if (!$(this).hasClass('open')) {
            $(this).trigger('click');
        } else {
            var focused_option = $($(this).find('.list .option:focus')[0] || $(this).find('.list .option.selected')[0]);
            focused_option.prev().focus();
        }
        return false;
        // Esc
    } else if (event.keyCode == 27) {
        if ($(this).hasClass('open')) {
            $(this).trigger('click');
        }
        return false;
    }
});

// PlanningSlider
function eventsSlider() {
    var today = new Date();
    today.setHours(0, 0, 0, 0);
    var toStart = 0;
    var i = 0;
    $(".events-slider .event").each(function(index) {
        var date_event = new Date($(this).attr('date'));
        date_event.setHours(0, 0, 0, 0);
        if (date_event >= today) {
            toStart = i;
            return false;
        }
        i++;
    });

    const width = window.innerWidth;
    const bigBreakpoint = 991;
    const bigNbrOfSlides = 4;
    const mediumBreakpoint = 767;
    const mediumNbrOfSlides = 2;

    var offset = 0;
    if (width > bigBreakpoint) {
        offset = bigNbrOfSlides - 1;
    } else if (width > mediumBreakpoint) {
        offset = mediumNbrOfSlides - 1;
    }
    var initialSlide = toStart - offset;
    if (initialSlide < 0) {
        initialSlide = 0;
    }

    $('.events-slider').slick({
        infinite: false,
        slidesToShow: bigNbrOfSlides,
        slidesToScroll: 1,
        dots: false,
        arrows: true,
        initialSlide: initialSlide,
        prevArrow: $('.planning-section .prev-slide'),
        nextArrow: $('.planning-section .next-slide'),
        responsive: [{
                breakpoint: bigBreakpoint,
                settings: {
                    slidesToShow: mediumNbrOfSlides
                }
            },
            {
                breakpoint: mediumBreakpoint,
                settings: {
                    slidesToShow: 1
                }
            },
        ]
    });
}

// Form animation
function materialize() {
    $("input, textarea").focus(function() {
        $(this).parents('.field-group').addClass("is-active is-completed");
    });

    $("input, textarea").focusout(function() {
        if ($(this).val() === "")
            $(this).parents('.field-group').removeClass("is-completed");
        $(this).parents('.field-group').removeClass("is-active");
    });

    $("input, textarea").each(function() {
        if ($(this).val() != "") {
            $(this).parents('.field-group').addClass("is-completed");
            $(this).parents('.field-group').addClass("is-active");
        }
    });
}

//Menu Mobile
function menuMobile() {
    // Menu mobile
    $('.menuburger').on('click', function() {
        if ($(this).hasClass('active')) {
            $('.main-nav .main-nav-list').fadeOut();
            $(this).removeClass('active');
            $('body, html').removeClass('no-scroll');

            // Remove
            $('.main-nav .main-nav-list .mobile-menu-footer').remove();
        } else {
            $('.main-nav .main-nav-list').css("display", "flex")
                .hide()
                .fadeIn();
            $(this).addClass('active');
            $('body, html').addClass('no-scroll');
        }
    });

    $('.menuburger--closer').on('click', function() {
        if ($('.menuburger').hasClass('active')) {
            $('.main-nav .main-nav-list').fadeOut();
            $('.menuburger').removeClass('active');
            $('body, html').removeClass('no-scroll');

            // Remove
            $('.main-nav .main-nav-list .mobile-menu-footer').remove();
        }
    });

    $(window).resize(function() {
        if (window.innerWidth > 991) {
            $('body, html').removeClass('no-scroll');
            $('.main-nav .main-nav-list').removeAttr("style");
            $('.main-nav .main-nav-list .mobile-menu-footer').remove();
            $('.menuburger').removeClass('active');
            $('.main-nav').removeClass("menu-mobile");
        } else {
            $('.main-nav').addClass("menu-mobile");
        }
    });

    if (window.innerWidth > 991) {
        $('.main-nav').removeClass("menu-mobile");
    } else {
        $('.main-nav').addClass("menu-mobile");
    }

    $(document).on('click', '.main-nav.menu-mobile .main-nav-list li.has-sub a', function(e) {
        e.preventDefault();
        if ($(this).parents('li.has-sub').hasClass('sub-open')) {
            $(this).siblings('.sub-nav').slideUp();
            $(this).parents('li.has-sub').removeClass('sub-open');
        } else {
            $(this).parents('.main-nav-list').find('li.has-sub').removeClass('sub-open');
            $(this).parents('.main-nav-list').find('li.has-sub .sub-nav').slideUp();
            $(this).parents('li.has-sub').addClass('sub-open');
            $(this).siblings('.sub-nav').slideDown();
        }
    });

}


$(window).on('load', function() {});

$(document).ready(function() {

    stickymenu();
    create_custom_dropdowns();
    materialize();
    eventsSlider();
    menuMobile();

    document.querySelectorAll('a[href^="#"]').forEach(anchor => {
        anchor.addEventListener('click', function(e) {
            e.preventDefault();

            document.querySelector(this.getAttribute('href')).scrollIntoView({
                behavior: 'smooth'
            });
        });
    });
});
