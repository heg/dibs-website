<?php
use Aptoma\Twig\Extension\MarkdownEngine;
use Aptoma\Twig\Extension\MarkdownExtension;
use Dibs\Site\Models\SpecialSection;
use Dibs\Site\Models\HomeInfo;
use Dibs\Site\Models\Partners;
use Dibs\Site\Models\PlanningDates;
use Dibs\Site\Models\Publications;
use Dibs\Site\Models\TeamMembers;
use Slim\Factory\AppFactory;
use Slim\Views\Twig;
use Slim\Views\TwigMiddleware;

require __DIR__ . '/../vendor/autoload.php';

$dotenv = Dotenv\Dotenv::createImmutable(__DIR__ . '/..');
$dotenv->load();

// Create App
$app = AppFactory::create();

// Create Twig
$twig = Twig::create(__DIR__ . '/../templates', [
    'cache' => false, //  __DIR__ . '/../cache',
]);

$engine = new MarkdownEngine\MichelfMarkdownEngine();

$twig->addExtension(new MarkdownExtension($engine));

// Add Twig-View Middleware
$app->add(TwigMiddleware::create($app, $twig));

$homeInfo       = new HomeInfo();
$specialSection = new SpecialSection();
$teamMembers    = new TeamMembers();
$planningDates  = new PlanningDates();
$publications   = new Publications();
$partners       = new Partners();

$app->get('/', function ($request, $response, $args) use ($homeInfo, $specialSection, $teamMembers, $planningDates, $publications, $partners) {
    $view = Twig::fromRequest($request);

    $homeInfo       = $homeInfo->find();
    $specialSection = $specialSection->find();
    $teamMembers    = $teamMembers->find();
    $planningDates  = $planningDates->find();
    $publications   = $publications->find();
    $partners       = $partners->find();

    return $view->render($response, 'home.html.twig', [
        'specialSection' => $specialSection,
        'homeInfo'       => $homeInfo,
        'teamMembers'    => $teamMembers,
        'planningDates'  => $planningDates,
        'publications'   => $publications,
        'partners'       => $partners,
        'base_api_url'   => $_ENV['API_HOST'],
        'page_name'      => 'Accueil',
    ]);
})->setName('home');

$errorMiddleware = $app->addErrorMiddleware(true, true, true);

$app->run();
