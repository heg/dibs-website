<!-- Footer start -->
<footer>
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-lg-4">
                    <h5>Contact</h5>
                    <p>
                        <b>HEG-Genève</b><br>
                        Campus Battelle<br>
                        Rue de Tambourine 17<br>
                        1227 Carouge
                    </p>
                </div>
                <div class="col-md-6 col-lg-4">
                    <h5>Direct access</h5>
                    <ul>
                        <li>
                            <a href="#">Contact</a>
                        </li>
                        <li>
                            <a href="#">Publications</a>
                        </li>
                        <li>
                            <a href="#">DIBS</a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="logo-footer text-right">
                <a href="https://www.hesge.ch/geneve/" target="_blank">
                    <img src="img/logo_hes-so-ge.svg" alt="HES-SO GE">
                </a>
            </div>
            <div class="sub-footer">
                <div class="row">
                    <div class="col-lg-6">
                        <ul>
                            <li>
                                <a href="#">About</a>
                            </li>
                            <li>
                                <a href="#">Press</a>
                            </li>
                            <li>
                                <a href="#">Privacy</a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-lg-6 text-right">
                        © HEG-Genève - Haute école de gestion de Genève 
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <a href="#" class="back-to-top">
        <i class="far fa-angle-up"></i>
    </a>
    <!-- Footer end -->
    
    <!-- Jquery -->
    <script src="js/vendor/jquery-3.5.1.min.js"></script>
    <!-- Slider Slick JS -->
    <script src="js/vendor/slick.min.js"></script>
    <!-- Main -->
    <script src="js/dibs.js"></script>
  </body>
</html>