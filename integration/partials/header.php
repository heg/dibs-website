<!doctype html>
<html lang="fr">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Adobe font -->
    <link rel="stylesheet" href="https://use.typekit.net/fpb8dgo.css">

    <!-- Bootstrap CSS -->
    <!--<link rel="stylesheet" href="css/vendor/bootstrap-reboot.min.css">-->
    <link rel="stylesheet" type="text/css" href="css/vendor/bootstrap-grid.min.css">
    <!-- Fontawesome CSS -->
    <link rel="stylesheet" type="text/css" href="css/vendor/all.min.css"/>
    <!-- Main CSS -->
    <link rel="stylesheet" type="text/css" href="css/dibs.css"/>
    <!-- Favicon -->
    <link rel="icon" type="image/png" href="img/favicon.png" />

    <title>DIBS<?php if (!empty($title)) echo ' | '.$title; ?></title>
  </head>
  <body>

    <!-- Header start -->
    <header>
        <nav class="main-nav">
            <a href="/" target="_blank" class="logo order-0">
                <img src="img/logo_dibs.png" alt="Logo DIBS">
            </a>
            <div class="main-nav-list order-2">
                <ul>
                    <li>
                        <a href="#activities">Activities</a>
                    </li>
                    <li>
                        <a href="#team">Team</a>
                    </li>
                    <li>
                        <a href="#planning">Planning</a>
                    </li>
                    <li>
                        <a href="#publications">Publications</a>
                    </li>
                    <li>
                        <a href="#partners">Partners</a>
                    </li>
                    <li>
                        <a href="#contact">Contact</a>
                    </li>
                </ul>
            </div>

            <div class="mobile-tools order-2">

                <!-- Menu burger btn start -->
                <div class="menuburger">
                    <span></span>
                    <span></span>
                    <span></span>
                </div>
                <!-- Menu burger btn end -->
            </div>
        </nav>
    </header>
    <div class="overlay"></div>
    <!-- Header end -->