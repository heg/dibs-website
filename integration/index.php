<?php include 'partials/header.php' ?>


    <!-- Main content start -->
    <div class="main-content">
        <!-- Home-banner section start -->
        <section class="home-banner">
            <div class="home-banner-content">
                <div class="home-banner-image" style="background-image: url(img/home_background.jpg);">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-8 col-xl-8">
                                <div class="home-banner-caption">
                                    <span class="page-subtitle">Research Project</span>
                                    <h1>Digital individual benefit statement</h1>
                                </div>
                            </div>
                            <div class="home-dibs-assistant-block col-lg-4 col-xl-4">
                                <div class="text-white">
                                        <h2>DIBS Assistant</h2>
                                        <div class="row margin-top-30">
                                            <div class="col-md-12">
                                                <label>I am</label>
                                                <select>
                                                    <option>Choose an option</option>
                                                    <option>Option 1</option>
                                                    <option>Option 2</option>
                                                    <option>Option 3</option>
                                                </select>
                                            </div>
                                            <div class="col-md-12">
                                                <label>I want</label>
                                                <select>
                                                    <option>Choose an option</option>
                                                    <option>Option 1</option>
                                                    <option>Option 2</option>
                                                    <option>Option 3</option>
                                                </select>
                                            </div>
                                        </div>
                                        <button class="btn red-btn full-btn red-btn-hover-white margin-top-30">GO</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Home-slider section end -->

        <!-- Activities section start -->
        <section id="activities" class="activites-section">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h2>Activities</h2>
                        <div class="content">
                            <img src="https://dummyimage.com/400x500/cccccc/969696.jpg" class="image-align-left">
                            <h3>How can you find your future individual benefit right now?</h3>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce hendrerit imperdiet egestas. Nulla facilisi. Praesent congue sapien at erat varius ornare. Aliquam lacus ante, tempus nec euismod eget, condimentum ac odio. Suspendisse volutpat rutrum ante quis dignissim. Aliquam libero tortor, sagittis a tempus sit amet, laoreet at sem. Aliquam erat volutpat.</p>
                            <p>In malesuada elit lorem, in molestie augue facilisis a. Nam at faucibus libero. Maecenas in justo odio. Donec placerat ante nulla, id efficitur mi gravida eget. Donec eleifend lobortis ultrices.</p>
                            <p>Quisque venenatis dignissim suscipit. Sed ut nunc quis felis dapibus ultrices sit amet feugiat lacus. Integer ac augue dignissim elit malesuada suscipit. Vestibulum pulvinar ex fringilla nisi fringilla, nec fringilla magna porta. Nulla molestie pharetra est, ut aliquam ipsum tristique at.</p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Activities section end -->

        <!-- Team section start -->
        <section id="team" class="team-section gray-bg">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h2>Core team of the project</h2>
                        <div class="content">
                            <div class="row">
                                <div class="intro col-md-6">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce hendrerit imperdiet egestas. Nulla facilisi.</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="team-member col-md-6">
                                    <img class="member-picture" src="https://dummyimage.com/110x110/cccccc/969696.jpg">
                                    <div class="member-infos">
                                        <div class="member-name">Catherine EQUEY BALZLI</div>
                                        <div class="member-job">Project director, main researcher</div>
                                    </div>
                                </div>
                                <div class="team-member col-md-6">
                                    <img class="member-picture" src="https://dummyimage.com/110x110/cccccc/969696.jpg">
                                    <div class="member-infos">
                                        <div class="member-name">Catherine EQUEY BALZLI</div>
                                        <div class="member-job">Project director, main researcher</div>
                                    </div>
                                </div>
                                <div class="team-member col-md-6">
                                    <img class="member-picture" src="https://dummyimage.com/110x110/cccccc/969696.jpg">
                                    <div class="member-infos">
                                        <div class="member-name">Catherine EQUEY BALZLI</div>
                                        <div class="member-job">Project director, main researcher</div>
                                    </div>
                                </div>
                                <div class="team-member col-md-6">
                                    <img class="member-picture" src="https://dummyimage.com/110x110/cccccc/969696.jpg">
                                    <div class="member-infos">
                                        <div class="member-name">Catherine EQUEY BALZLI</div>
                                        <div class="member-job">Project director, main researcher</div>
                                    </div>
                                </div>
                            </div>
                            <div class="align-center margin-top-100"><a class="btn red-btn" href="#contact">Contact the team</a></div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Team section end -->

        <!-- Planning section start -->
        <section id="planning" class="planning-section">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h2>Planning</h2>
                        <div class="content">
                            <div class="row">
                                <div class="intro col-md-6">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce hendrerit imperdiet egestas. Nulla facilisi.</p>
                                </div>
                            </div>
                            <div class="row position-relative margin-top-60">
                                <div class="events-slider col-md-12">
                                    <div id="event-0" date="2021-04-21" class="event">
                                        <div class="event-name">KICK-OFF</div>
                                        <div class="event-description"><p>Partners presentation<br>Official launch</p></div>
                                        <div class="event-date">21.04.2021</div>
                                    </div>
                                    <div id="event-1" date="2021-08-11" class="event">
                                        <div class="event-name">KICK-OFF</div>
                                        <div class="event-description"><p>Partners presentation<br>Official launch</p></div>
                                        <div class="event-date">11.08.2021</div>
                                    </div>
                                    <div id="event-2" date="2021-09-02" class="event">
                                        <div class="event-name">KICK-OFF</div>
                                        <div class="event-description"><p>Partners presentation<br>Official launch<br>Official launch</p></div>
                                        <div class="event-date">02.09.2021</div>
                                    </div>
                                    <div id="event-3" date="2021-11-11" class="event">
                                        <div class="event-name">KICK-OFF</div>
                                        <div class="event-description"><p>Partners presentation<br>Official launch</p></div>
                                        <div class="event-date">11.11.2021</div>
                                    </div>
                                    <div id="event-4" date="2021-12-18" class="event">
                                        <div class="event-name">KICK-OFF</div>
                                        <div class="event-description"><p>Partners presentation<br>Official launch</p></div>
                                        <div class="event-date">18.12.2021</div>
                                    </div>
                                    <div id="event-5" date="2022-01-11" class="event">
                                        <div class="event-name">KICK-OFF</div>
                                        <div class="event-description"><p>Partners presentation<br>Official launch</p></div>
                                        <div class="event-date">11.01.2022</div>
                                    </div>
                                    <div id="event-6" date="2022-02-28" class="event">
                                        <div class="event-name">KICK-OFF</div>
                                        <div class="event-description"><p>Partners presentation<br>Official launch</p></div>
                                        <div class="event-date">28.02.2022</div>
                                    </div>
                                    <div id="event-6" date="2022-03-03" class="event">
                                        <div class="event-name">KICK-OFF</div>
                                        <div class="event-description"><p>Partners presentation<br>Official launch</p></div>
                                        <div class="event-date">03.03.2022</div>
                                    </div>
                                    <div id="event-6" date="2022-03-10" class="event">
                                        <div class="event-name">KICK-OFF</div>
                                        <div class="event-description"><p>Partners presentation<br>Official launch</p></div>
                                        <div class="event-date">10.03.2022</div>
                                    </div>
                                    <div id="event-6" date="2022-05-25" class="event">
                                        <div class="event-name">KICK-OFF</div>
                                        <div class="event-description"><p>Partners presentation<br>Official launch</p></div>
                                        <div class="event-date">25.05.2022</div>
                                    </div>
                                    <div id="event-6" date="2022-05-26" class="event">
                                        <div class="event-name">KICK-OFF</div>
                                        <div class="event-description"><p>Partners presentation<br>Official launch</p></div>
                                        <div class="event-date">26.05.2022</div>
                                    </div>
                                    <div id="event-6" date="2022-05-27" class="event">
                                        <div class="event-name">KICK-OFF</div>
                                        <div class="event-description"><p>Partners presentation<br>Official launch</p></div>
                                        <div class="event-date">27.05.2022</div>
                                    </div>
                                </div>
                                <div class="action-slide prev-slide"></div>
                                <div class="action-slide next-slide"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Planning section end -->

        <!-- Publications section start -->
        <section id="publications" class="publications-section gray-bg">
            <div class="container px-4">
                <div class="row">
                    <div class="col-md-12">
                        <h2>Publications</h2>
                        <div class="content">
                            <div class="row">
                                <div class="intro col-md-6">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce hendrerit imperdiet egestas. Nulla facilisi.</p>
                                </div>
                            </div>
                            <div class="row publi-row">
                                <div class="publi col-md-3 col-sm-6 px-5">
                                    <a href="#">
                                        <img class="publi-image" src="https://dummyimage.com/270x367/cccccc/969696.jpg">
                                        <div class="publi-icon p-i-pdf"></div>
                                        <div class="publi-title">KICK-OFF PV</div>
                                        <div class="publi-date">04.02.2022</div>
                                    </a>
                                </div>
                                <div class="publi col-md-3 col-sm-6 px-5">
                                    <a href="#">
                                        <img class="publi-image" src="https://dummyimage.com/270x367/cccccc/969696.jpg">
                                        <div class="publi-icon p-i-word"></div>
                                        <div class="publi-title">KICK-OFF PV</div>
                                        <div class="publi-date">04.02.2022</div>
                                    </a>
                                </div>
                                <div class="publi col-md-3 col-sm-6 px-5">
                                    <a href="#">
                                        <img class="publi-image" src="https://dummyimage.com/270x367/cccccc/969696.jpg">
                                        <div class="publi-icon p-i-download"></div>
                                        <div class="publi-title">KICK-OFF PV</div>
                                        <div class="publi-date">04.02.2022</div>
                                    </a>
                                </div>
                                <div class="publi col-md-3 col-sm-6 px-5">
                                    <a href="#">
                                        <img class="publi-image" src="https://dummyimage.com/270x367/cccccc/969696.jpg">
                                        <div class="publi-icon p-i-video"></div>
                                        <div class="publi-title">KICK-OFF PV</div>
                                        <div class="publi-date">04.02.2022</div>
                                    </a>
                                </div>
                            </div>
                            <div class="align-center margin-top-100"><a class="btn red-btn" href="#contact">See all documents</a></div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Publications section end -->

        <!-- Partners section start -->
        <section id="partners" class="partners-section">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h2>Partners</h2>
                        <div class="content">
                        <div class="row">
                                <div class="intro col-md-6">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce hendrerit imperdiet egestas. Nulla facilisi.</p>
                                </div>
                            </div>
                            <div class="row partners-row">
                                <div class="partner col-md-3 col-sm-6 px-5">
                                    <a href="#">
                                        <img class="partner-logo" src="https://dummyimage.com/270x190/cccccc/969696.jpg">
                                    </a>
                                </div>
                                <div class="partner col-md-3 col-sm-6 px-5">
                                    <a href="#">
                                        <img class="partner-logo" src="https://dummyimage.com/270x190/cccccc/969696.jpg">
                                    </a>
                                </div>
                                <div class="partner col-md-3 col-sm-6 px-5">
                                    <a href="#">
                                        <img class="partner-logo" src="https://dummyimage.com/270x190/cccccc/969696.jpg">
                                    </a>
                                </div>
                                <div class="partner col-md-3 col-sm-6 px-5">
                                    <a href="#">
                                        <img class="partner-logo" src="https://dummyimage.com/270x190/cccccc/969696.jpg">
                                    </a>
                                </div>
                                <div class="partner col-md-3 col-sm-6 px-5">
                                    <a href="#">
                                        <img class="partner-logo" src="https://dummyimage.com/270x190/cccccc/969696.jpg">
                                    </a>
                                </div>
                                <div class="partner col-md-3 col-sm-6 px-5">
                                    <a href="#">
                                        <img class="partner-logo" src="https://dummyimage.com/270x190/cccccc/969696.jpg">
                                    </a>
                                </div>
                                <div class="partner col-md-3 col-sm-6 px-5">
                                    <a href="#">
                                        <img class="partner-logo" src="https://dummyimage.com/270x190/cccccc/969696.jpg">
                                    </a>
                                </div>
                                <div class="partner col-md-3 col-sm-6 px-5">
                                    <a href="#">
                                        <img class="partner-logo" src="https://dummyimage.com/270x190/cccccc/969696.jpg">
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Partners section end -->

        <!-- Contact section start -->
        <section id="contact" class="contact-section gray-bg">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h2>Contact</h2>
                        <div class="content">
                        <div class="row">
                                <div class="intro col-md-6">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce hendrerit imperdiet egestas. Nulla facilisi.</p>
                                </div>
                            </div>
                            <form>
                                <div class="row">
                                
                                    <div class="col-md-6">
                                        <div class="field-group">
                                            <label>First name *</label>
                                            <input type="text">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="field-group">
                                            <label>Last name *</label>
                                            <input type="text">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="field-group">
                                            <label>Email *</label>
                                            <input type="text">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="field-group">
                                            <label>Phone</label>
                                            <input type="text">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                    <div class="field-group">
                                        <label>Message *</label>
                                        <textarea></textarea>
                                    </div>
                                    <div class="col-md-12 margin-top-30">
                                        <div class="checkbox-group">
                                            <label><input type="checkbox" required="required">By submitting this form I agree that my personal data can be used as part of my contat request. *</a></label>
                                        </div>
                                    </div>
                                    <div class="col-md-12 align-center margin-top-60">
                                        <button type="submit" class="btn red-btn">Send the message</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Contact section end -->

    </div>
    <!-- Main content end -->

<?php include 'partials/footer.php' ?>