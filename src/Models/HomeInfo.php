<?php

namespace Dibs\Site\Models;

use Dibs\Site\ApiModel;

class HomeInfo extends ApiModel
{
    CONST ELEMENTS = 'home-info';
}
