<?php

namespace Dibs\Site\Models;

use Dibs\Site\ApiModel;

class Publications extends ApiModel
{
    CONST ELEMENTS = 'dibs-publications';
    CONST SORT = ['date:asc'];
}
