<?php

namespace Dibs\Site\Models;

use Dibs\Site\ApiModel;

class PlanningDates extends ApiModel
{
    CONST ELEMENTS = 'dibs-planning-dates';
    CONST SORT = ['date:asc'];
}
