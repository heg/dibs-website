<?php

namespace Dibs\Site\Models;

use Dibs\Site\ApiModel;

class SpecialSection extends ApiModel
{
    CONST ELEMENTS = 'special-section';
}
