<?php

namespace Dibs\Site\Models;

use Dibs\Site\ApiModel;

class TeamMembers extends ApiModel
{
    CONST ELEMENTS = 'dibs-team-members';
    CONST SORT = ['order:asc'];
}
