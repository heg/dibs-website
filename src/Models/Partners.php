<?php

namespace Dibs\Site\Models;

use Dibs\Site\ApiModel;

class Partners extends ApiModel
{
    CONST ELEMENTS = 'dibs-partners';
    CONST SORT = ['name:asc'];
}
