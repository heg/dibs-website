<?php

namespace Dibs\Site;

use \GuzzleHttp\Client as GuzzleClient;

class ApiModel
{
    CONST ELEMENTS = '';
    CONST SORT = [];

    public function __construct()
    {
    }

    public function find()
    {
        $client = new GuzzleClient([
            // Base URI is used with relative requests
            'base_uri' => $_ENV['API_HOST'],
        ]);

        $response = $client->request('GET', '/api/' . get_called_class()::ELEMENTS, [
            'query' => [
                'populate' => '*',
                'sort' => get_called_class()::SORT
            ],
        ]);
        $code     = $response->getStatusCode();   // 200
        $reason   = $response->getReasonPhrase(); // OK
        $response = json_decode((string) $response->getBody(), true);

        $response = array_map(function ($result) {
            return $result;
        }, $response['data']);

        return $response;
    }

}
